#ifndef ENUMS_H
#define ENUMS_H

class Enums {
public:
    enum Action {
        MoveLeft,
        MoveRight,
        Shoot
    };

    enum DefenderSize {
        Tiny,
        Medium,
        Large,
        Giant
    };
};

#endif // ENUMS_H
