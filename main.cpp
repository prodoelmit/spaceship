#include "battlesphere.h"
#include <QApplication>
#include "app.h"

int main(int argc, char *argv[])
{
//    QApplication a(argc, argv);
    App a(argc, argv);
    si::BattleSphere w;
    a.setBs(&w);
    w.show();

    return a.exec();
}
