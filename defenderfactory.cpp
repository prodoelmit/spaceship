#include "defenderfactory.h"

DefenderFactory::DefenderFactory()
{

}

Defender *DefenderFactory::createDefender(Enums::DefenderSize s, QSize boardSize)
{

    Defender* def = new Defender();
    QSize size;
    switch (s) {
    case Enums::Tiny:
        size = QSize(40,10);
        break;
    case Enums::Giant:
        size = QSize(400, 100);
        break;
        //....
    }
    def->setSize(size);

    QPoint initialPosition(boardSize.width() / 2. - size.width() / 2.,
                           boardSize.height() - size.height() ); // dunno where we'll place it yet
    def->setPos(initialPosition);


    return def;
}
