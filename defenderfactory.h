#ifndef DEFENDERFACTORY_H
#define DEFENDERFACTORY_H
#include "defender.h"


class DefenderFactory
{
public:
    DefenderFactory();

    static Defender* createDefender(Enums::DefenderSize s, QSize boardSize);
};

#endif // DEFENDERFACTORY_H
