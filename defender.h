#ifndef DEFENDER_H
#define DEFENDER_H
#include <QPixmap>
#include "enums.h"
#include <QPoint>
#include <QSize>


class Defender
{
public:
    Defender();

    void performAction(Enums::Action a);

    QSize size() const;
    void setSize(const QSize &size);

    QPoint pos() const;
    void setPos(const QPoint &pos);

    void update(QPainter &painter);

private:
    QPoint m_pos;

    QSize m_size;

    QPixmap m_pixmap;


};

#endif // DEFENDER_H
