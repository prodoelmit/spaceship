#-------------------------------------------------
#
# Project created by QtCreator 2017-03-15T13:23:41
#
#-------------------------------------------------

QT       += core gui
QT       += multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Spaceship
TEMPLATE = app

CONFIG += c++11


SOURCES += main.cpp\
    battlesphere.cpp \
    app.cpp \
    settings.cpp \
    defender.cpp \
    defenderfactory.cpp

HEADERS  += \
    battlesphere.h \
    app.h \
    settings.h \
    enums.h \
    defender.h \
    defenderfactory.h

RESOURCES += \
    resources.qrc

OTHER_FILES +=
