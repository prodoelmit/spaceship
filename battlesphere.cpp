#include "battlesphere.h"
#include "defenderfactory.h"

namespace si {

BattleSphere::BattleSphere(QWidget *parent) :
    QDialog(parent)
    , m_defender(0)
    , sound(":/s;ounds/explosion_x.wav")
{

        bullet.load(":/images/fireball.png");
        setStyleSheet("background-color: #000000;");
        this->resize(600, 400);
        update();

        m_defender = DefenderFactory::createDefender(Enums::Giant, this->size());

        timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()), this, SLOT(nextFrame()));
        timer->start(32);

        // Is it allowed to do somethin here?


    }

    BattleSphere::~BattleSphere() {
        delete timer;
    }

    void BattleSphere::paintEvent(QPaintEvent *event) {
        QPainter painter(this);
//        painter.drawPixmap(dx, dy, defender);
        painter.drawPixmap(bx, by, bullet);
        if (m_defender)
            m_defender->update(painter);
    }

    void BattleSphere::nextFrame() {
        // animate the defender
        int maxX = this->width()-defender.width();
        dx += ds;
        if(dx >= maxX){
            dx = (2*maxX)-dx;
            ds *= -1;
        } else if (dx <= 0) {
            dx *= -1;
            ds *= -1;
        }

        // shoot or animate the bullet
        if(by <= -100){
            bx = dx + (defender.width()/2) - (bullet.width()/2);
            by = dy - bullet.height();
//            sound.play();
        } else {
            by -= bs;
        }

        update();

    }

} // end namespace si
