#ifndef APP_H
#define APP_H
#include <QApplication>
#include "battlesphere.h"
#include "settings.h"


class App: public QApplication
{
    Q_OBJECT
public:
    App(int& argc, char** argv);



    si::BattleSphere *bs() const;
    void setBs(si::BattleSphere *bs);

    si::Settings *settings() const;
    void setSettings(si::Settings *settings);

private:
    si::BattleSphere* m_bs;

    si::Settings* m_settings;



};

#define APP dynamic_cast<App*>(qApp)
#define BS APP->bs()

#endif // APP_H
