#include "app.h"

App::App(int &argc, char **argv)
    : QApplication(argc, argv)
{

}


si::BattleSphere *App::bs() const
{
    return m_bs;
}

void App::setBs(si::BattleSphere *bs)
{
    m_bs = bs;
}

si::Settings *App::settings() const
{
    return m_settings;
}

void App::setSettings(si::Settings *settings)
{
    m_settings = settings;
}
