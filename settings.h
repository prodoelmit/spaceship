#ifndef SETTINGS_H
#define SETTINGS_H
#include <QString>
#include <enums.h>
#include <QVector>


namespace si {
class Settings
{
public:
    Settings(QString filename); // filename is just some plain-text file




    QVector<Enums::Action> actions() const;

private:
    QVector<Enums::Action> m_actions;


};
}

#endif // SETTINGS_H
