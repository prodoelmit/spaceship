#include "defender.h"
#include <QPainter>
#include "app.h"

Defender::Defender():
m_size(QSize(20,20))
{
    m_pixmap.load(":/images/defender.png");

}

void Defender::performAction(Enums::Action a)
{
    switch (a) {
    case Enums::MoveLeft:
        m_pos += QPoint(-1, 0);
        break;
    case Enums::MoveRight:
        m_pos += QPoint(1, 0);
        break;
    case Enums::Shoot:
        //shoot
        break;
    }

}

QSize Defender::size() const
{
    return m_size;
}

void Defender::setSize(const QSize &size)
{
    m_size = size;
    m_pixmap = QPixmap(":/images/defender.png").scaled(m_size);

}

QPoint Defender::pos() const
{
    return m_pos;
}

void Defender::setPos(const QPoint &pos)
{
    m_pos = pos;
}

void Defender::update(QPainter& painter)
{

//    QPainter painter;
//    painter.begin(BS);
        painter.drawPixmap(m_pos.x(), m_pos.y(), m_pixmap);

//        painter.drawPixmap(m_pos.x(), m_pos.y(),
//                           m_size.width(),
//                           m_size.height(),
//                           m_pixmap,
//                            0,
//                           0,
//                           m_pixmap.width(),
//                           m_pixmap.height()
//                           );

//    painter.drawRect(0, 0, m_size.width(), m_size.height());
//        painter.end();

}
